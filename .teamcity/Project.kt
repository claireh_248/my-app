import _Self.buildTypes.buildPipeline.BuildAndTestVerification
import _Self.vcsRoots.Master
import _Self.vcsRoots.Feature
import jetbrains.buildServer.configs.kotlin.Project
import _Self.templates.BuildAndTestVerificationTemplate
object Project : Project({
    //VCS Roots 
    vcsRoot(Master)
    vcsRoot(Feature)

    template(BuildAndTestVerificationTemplate)

    buildType(BuildAndTestVerification)
    
})
