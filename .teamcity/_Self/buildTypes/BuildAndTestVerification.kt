package _Self.buildTypes.buildPipeline

import _Self.vcsRoots.Master
import jetbrains.buildServer.configs.kotlin.AbsoluteId
import jetbrains.buildServer.configs.kotlin.BuildType
import _Self.templates.BuildAndTestVerificationTemplate

object BuildAndTestVerification : BuildType({
    templates(BuildAndTestVerificationTemplate)
    name = "Main Build and Test Verification"
    vcs {
        root(Master)
    }
})