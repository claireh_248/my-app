package _Self.templates

import jetbrains.buildServer.configs.kotlin.Template
import jetbrains.buildServer.configs.kotlin.buildSteps.exec

object BuildAndTestVerificationTemplate : Template({
    name = "Build and Test Verification"

    steps {
        exec {
            name = "npm :: install"
            id = "RUNNER_1"
            path = "npm"
            arguments = "install"
        }
        exec {
            id = "RUNNER_3"
            path = "npm"
            arguments = "run build"
        }
        exec {
            name = "Publish to Artifactory"
            id = "RUNNER_4"
            path = "echo"
            arguments = """"publish to artifactory""""
        }
    }
})
